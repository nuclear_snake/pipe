using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// O gameobject é necessário estar ativado no editor para ser injetado nas classes
/// </summary>
public class ActionContextGUI : MonoBehaviour
{
    public RawImage Symbol;
    public TextMeshProUGUI Key;
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
