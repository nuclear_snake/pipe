using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
	[SerializeField]
	private Transform LookTo;

	[Min(0f)]
	public float MoveSmooth = 0.3f;


	public Vector3 Offset;

	[Header("Debug"), SerializeField]
	private bool _preview;

	//Dados de coordenadas esféricas
	/// <summary>
	/// Distância radial
	/// </summary>
	private float _r;
	/// <summary>
	/// Ângulo polar
	/// </summary>
	private float _phi;
	/// <summary>
	/// Ângulo azimutal
	/// </summary>
	private float _theta;

	private Vector3 _velocity;

	/// <summary>
	/// Offset em relação ao Target
	/// </summary>
	public Vector3 SphericalCoordinate => new Vector3 // [!] Botar em cache
	{
		x = _r * Mathf.Sin(_theta) * Mathf.Cos(_phi),
		y = _r * Mathf.Sin(_theta) * Mathf.Sin(_phi),
		z = _r * Mathf.Cos(_theta),
	};

	/// <summary>
	/// Posição onde o objeto deve estar em relação ao alvo
	/// </summary>
	public Vector3 TargetPosition => SphericalCoordinate + LookTo.transform.position + Offset;

	// Start is called before the first frame update
	private void OnEnable()
	{
		CalcCoordinatesData();

	}

	private void CalcCoordinatesData()
	{

		var targetPoint = transform.position - LookTo.position;

		_r = Vector3.Distance(transform.position, LookTo.position);
		_phi = Mathf.Atan2(targetPoint.y, targetPoint.x);
		_theta = Mathf.Acos(targetPoint.z / _r);

		Debug.Log("Calculado");
	}

	// Update is called once per frame
	void Update()
	{

		transform.position = Vector3.SmoothDamp(transform.position, TargetPosition, ref _velocity, MoveSmooth, float.MaxValue, Time.deltaTime);
		transform.LookAt(LookTo);



	}

	private void OnDrawGizmosSelected()
	{
		if (LookTo != null)
		{

			// Simulando deslocamento
			// Não pode acontecer durante o jogo
			if (!Application.isPlaying)
				CalcCoordinatesData();
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(TargetPosition, LookTo.position);
		}
	}

	private void OnValidate()
	{
		if (LookTo != null)
		{
			transform.LookAt(LookTo);
		}
	}
}
