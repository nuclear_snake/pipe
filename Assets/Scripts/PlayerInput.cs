using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInput : MonoBehaviour {

	private MainControls _mainControls;
	public AgentMovement AgentMovement;

	private void Awake() {
		_mainControls = new MainControls();
	}

	private void OnEnable() {
		_mainControls.Gameplay.Jump.started += StartJump;
		_mainControls.Gameplay.Jump.canceled += ReleaseJump;
		_mainControls.Gameplay.Brake.started += Brake;
		_mainControls.Gameplay.Brake.canceled += ReleaseBrake;
		_mainControls.Enable();
	}
	private void OnDisable() {
		_mainControls.Gameplay.Jump.started -= StartJump;
		_mainControls.Gameplay.Jump.canceled -= ReleaseJump;
		_mainControls.Gameplay.Brake.started -= Brake;
		_mainControls.Gameplay.Brake.canceled -= ReleaseBrake;
		_mainControls.Disable();
	}

	private void Update() {


		AgentMovement.HorizontalMove = _mainControls.Gameplay.Move.ReadValue<float>();
		if (_mainControls.Gameplay.Jump.IsPressed() && !_mainControls.Gameplay.Jump.WasPressedThisFrame()){
			AgentMovement.TryApplyAddictionalJumpForce();
			
		}

	}




	public void StartJump(InputAction.CallbackContext callback) {
		
		AgentMovement.TryJump();

	}

	public void ReleaseJump(InputAction.CallbackContext callback)
		=> AgentMovement.ReleaseJump();

	public void Brake(InputAction.CallbackContext callback)
		=> AgentMovement.TryBrake();
	public void ReleaseBrake(InputAction.CallbackContext callback)
		=> AgentMovement.ReleaseBrake();
}