using System;
using UnityEngine;

[Serializable]
public struct AgentGroundCheckProperties
{
    [Min(0f)]
    public float Distance;
    [Range(0f, 90f)]
    public float GroundedMaxAngle;
    public Vector3 StartOffset;


}







