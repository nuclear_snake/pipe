using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextAction : MonoBehaviour
{
	public Collider Trigger;
	private ActionContextGUI _actionContextGUI;
	public event Action<Collider> OnEnter;
	public event Action<Collider> OnAction;
	public event Action<Collider> OnLeave;
	private bool _actived;
	private MainControls _mainControls;

	private void OnEnable() {
		_mainControls.Enable();
	}

	private void OnDisable() {
		_mainControls.Disable();

	}

	private void OnTriggerEnter(Collider other) {
		_actionContextGUI.gameObject.SetActive(true);
		Debug.Log("Entrou");
		OnEnter?.Invoke(other);
	}

	private void OnTriggerExit(Collider other) {
		_actionContextGUI.gameObject.SetActive(false);
		OnLeave?.Invoke(other);
	}

	private void OnTriggerStay(Collider other) {
		if(_mainControls.Gameplay.ContextAction.WasReleasedThisFrame()){
			OnAction?.Invoke(other);
			_actived = true;
		}
		else
			_actived = false;

	}

	private void Awake() {
		_mainControls = new MainControls();
		_actionContextGUI = FindAnyObjectByType<ActionContextGUI>();
	}

	private void OnDrawGizmos() {
		if(_actived){
			Gizmos.matrix = transform.localToWorldMatrix;
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(Vector3.zero, 0.5f);
		}
			
	}
	
}
