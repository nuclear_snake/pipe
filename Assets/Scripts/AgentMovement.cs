using UnityEngine;

public class AgentMovement : MonoBehaviour
{

    public AgentSettings Settings;
    public SphereCollider Collider;
    public Rigidbody Rigidbody;
    public Transform Feet;
    
    private Observable<bool> _grounded;
    private bool _jumping;
    private Observable<bool> _braking;

    private float _velocityBeforeAir; //Criar limite de velidade no ar

    private float _horizontalMove;
    private float _additionalJumpCountdown;

    public float HorizontalMove { get => _horizontalMove; set => _horizontalMove = value; }
    public AgentMovementProperties MovementProperties => Settings.Movement;
    public AgentGroundCheckProperties GroundCheckProperties => Settings.GroundCheck;


    public void TryJump()
    {


        //Pulando do chão
        if (_grounded && !_jumping)
        {
            _additionalJumpCountdown = MovementProperties.AddictionalJumpDuration;

            _jumping = true;
            _grounded.Value = false;
            Rigidbody.AddForce(Vector3.up * MovementProperties.JumpForce, ForceMode.VelocityChange);

        }

    }

    /// <summary>
    /// Só funciona se estiver no estado de 'pulando'
    /// </summary>
    public void TryApplyAddictionalJumpForce()
    {
        //Adicionando impulso
        if (_jumping)
        {
            Rigidbody.AddForce(MovementProperties.AddictionalJumpForce * Time.deltaTime * Vector3.up, ForceMode.VelocityChange);
            Debug.Log(".");
        }
        else
            Debug.Log("pass");
    }
    public void ReleaseJump()
    {
        _jumping = false;

    }

    public void TryBrake()
    {
        if (_grounded)
        {
            _braking.Value = true;

        }
    }
    public void ReleaseBrake()
    {
        _braking.Value = false;
    }

    private void OnBrakeChange(bool willBrake)
    {
        if (willBrake)
            Rigidbody.angularDrag = MovementProperties.AngularDragOnBrake;
        else
            Rigidbody.angularDrag = MovementProperties.AngularDrag;
    }

    private void OnGroundedChange(bool willGround)
    {
        if (willGround)
        {
            Collider.material = MovementProperties.GroundedPhysicMaterial;
        }
        else
        {
            Collider.material = MovementProperties.AirPhysicMaterial;
            _velocityBeforeAir = Mathf.Abs(Rigidbody.velocity.x);
        }
    }

    private void OnEnable()
    {
        _braking.OnChange += OnBrakeChange;
        _grounded.OnChange += OnGroundedChange;
    }
    private void OnDisable()
    {
        _braking.OnChange -= OnBrakeChange;
        _grounded.OnChange -= OnGroundedChange;
    }

    private void Awake()
    {
        _grounded = new();
        _braking = new();

        Rigidbody.angularDrag = MovementProperties.AngularDrag;

    }
    void FixedUpdate()
    {

        //Movimentação baseada no atrito quando a bola gira
        if (!Mathf.Approximately(_horizontalMove, 0))
            Rigidbody.AddTorque(Vector3.back * HorizontalMove * MovementProperties.Speed * Time.fixedDeltaTime, ForceMode.Acceleration);

        //Estado de 'pulando'
        if (_jumping)
        {
            _additionalJumpCountdown -= Time.fixedDeltaTime;
            if (_additionalJumpCountdown <= 0f)
            {
                _jumping = false;
            }
        }
        // Estado de 'livre no ar'
        var origin = transform.position + Vector3.Scale(GroundCheckProperties.StartOffset, transform.lossyScale);
        var allHits = Physics.SphereCastAll(origin, Collider.radius * transform.lossyScale.x, Vector3.down, GroundCheckProperties.Distance * transform.lossyScale.y, Settings.CollidesWith);

        var grounded = false;
        for (int i = 0; i < allHits.Length; i++)
        {
            var hitIsValid = allHits[i].distance > 0f && allHits[i].point != Vector3.zero; //Hit inválido satisfaz estas condições ¯\_(ツ)_/¯
            if (hitIsValid && Vector3.Angle(allHits[i].normal, Vector3.up) <= GroundCheckProperties.GroundedMaxAngle)
            {
                Debug.DrawRay(allHits[i].point, allHits[i].normal, Color.yellow);
                grounded = true;
                break;
            }
        }

        _grounded.Value = grounded;

        //Estado de 'no chão'
        if (_grounded)
        {
            if (_braking)
            {
                Rigidbody.angularVelocity = Vector3.zero;
            }
        }

        // Estado de 'no ar'
        if (!_grounded.Value)
        {

            Rigidbody.AddForce(Vector3.right * HorizontalMove * MovementProperties.SpeedOnAir * Time.deltaTime, ForceMode.Acceleration);

            //Obtendo velocidade máxima permitida
            var xVelocity = Mathf.Clamp(Mathf.Abs(Rigidbody.velocity.x), 0f, Mathf.Min(MovementProperties.SpeedOnAir, _velocityBeforeAir));

            var factor = Rigidbody.velocity.x > 0f ? 1f : -1f;
            Rigidbody.velocity = new Vector3(xVelocity * factor, Rigidbody.velocity.y, Rigidbody.velocity.z);
        }

    }

    private void OnDrawGizmosSelected()
    {
        if (Settings != null && Collider != null)
        {
            Gizmos.color = Color.yellow;
            var startPosition = transform.position + Vector3.Scale(GroundCheckProperties.StartOffset, transform.lossyScale);
            var endPosition = startPosition + Vector3.down * GroundCheckProperties.Distance * transform.lossyScale.y;
            Gizmos.DrawWireSphere(startPosition, Collider.radius * transform.lossyScale.x);
            Gizmos.DrawLine(startPosition, endPosition);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(endPosition, Collider.radius * transform.lossyScale.x);
        }

    }
}







