using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class GenericSerializedDictionary<TKey, TValue, TDictionaryImpl> : IDictionary<TKey, TValue> where TDictionaryImpl : IDictionary<TKey, TValue>, new()
{
    private readonly TDictionaryImpl _dictionary = new();



    public TValue this[TKey key] { get => _dictionary[key]; set => _dictionary[key] = value; }

    public ICollection<TKey> Keys => _dictionary.Keys;

    public ICollection<TValue> Values => _dictionary.Values;

    public int Count => _dictionary.Count;

    public bool IsReadOnly => false;

    public void Add(TKey key, TValue value)
    {
        _dictionary.Add(key, value);
    }

    public void Add(KeyValuePair<TKey, TValue> item)
    {
        _dictionary.Add(item.Key, item.Value);
    }

    public void Clear()
    {
        _dictionary.Clear();
    }

    public bool Contains(KeyValuePair<TKey, TValue> item)
        => _dictionary.Contains(item);


    public bool ContainsKey(TKey key)
        => _dictionary.ContainsKey(key);

    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
        _dictionary.CopyTo(array, arrayIndex);
    }

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
        return _dictionary.GetEnumerator();
    }

    public bool Remove(TKey key)
    {
        return _dictionary.Remove(key);

    }

    public bool Remove(KeyValuePair<TKey, TValue> item)
    {
        return Remove(item);
    }

    public bool TryGetValue(TKey key, out TValue value)
    {
        return TryGetValue(key, out value);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _dictionary.GetEnumerator();
    }
}

public class SerializedDictionary<TKey, TValue> : GenericSerializedDictionary<TKey, TValue, Dictionary<TKey, TValue>>{

}