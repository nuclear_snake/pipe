using System;
using UnityEngine;

[Serializable]
public class Observable<T>
{
    [SerializeField]
    private T _value;

    public T Value
    {
        get => _value;
        set
        {
            if ((this._value is null && value is not null) || (this._value is not null && !this._value.Equals(value)))
            {
                //Debug.Log(".");
                this._value = value;
                OnChange?.Invoke(value);
            }
        }
    }

    public Observable() {
        _value = default;
     }
    public Observable(T startValue)
    {
        _value = startValue;
    }

    public event Action<T> OnChange;

    public static implicit operator T(Observable<T> instance)
    {
        return instance.Value;
    }
}
