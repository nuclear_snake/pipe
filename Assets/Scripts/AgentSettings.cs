using UnityEngine;

[CreateAssetMenu(fileName = "AgentSettings", menuName = "Pipe/AgentSettings")]
public class AgentSettings : ScriptableObject
{
    public LayerMask CollidesWith;
    public AgentMovementProperties Movement;
    public AgentGroundCheckProperties GroundCheck;
}







