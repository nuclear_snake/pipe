using System;
using UnityEngine;

[Serializable]
public struct AgentMovementProperties
{
    [Min(0f)]
    public float Speed;
    [Min(0f)]
    public float SpeedOnAir;
    [Min(0f)]
    public float JumpForce;
    [Min(0f)]
    public float AddictionalJumpForce;
    [Min(0f)]
    public float AddictionalJumpDuration;
    
    public float AngularDrag;
    [Min(0f), Tooltip("Eficiência do freio, quanto menor, melhor")]
    public float AngularDragOnBrake;
    public PhysicMaterial GroundedPhysicMaterial;
    public PhysicMaterial AirPhysicMaterial;

}







