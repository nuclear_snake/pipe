using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ButtonTrigger : MonoBehaviour
{

	public float Resistence;
	public Animator Animator;
	public Material MaterialOn, MaterialOff;
	public MeshRenderer Mesh;
	private bool _on;


	[ContextMenu(nameof(TurnOn))]
	public void TurnOn()
	{
		Debug.Log("Ligando");
		_on = true;
		Animator.SetBool("on", true);
		
		
	}

	[ContextMenu(nameof(TurnOff))]
	public void TurnOff()
	{
		Debug.Log("Desligando");
		_on = false;
		Animator.SetBool("on", false);
	}

	public void VisualOn(){
		Mesh.SetMaterials(new List<Material> { MaterialOn } );
	}

	public void VisualOff(){
		Mesh.SetMaterials(new List<Material> { MaterialOff } );
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.impulse.magnitude / Time.fixedDeltaTime < Resistence)
			return;

		if (_on)
			TurnOff();
		else
			TurnOn();



	}




}
