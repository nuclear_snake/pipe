using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

[CustomPropertyDrawer(typeof(Observable<>))]
public class ObservableDrawer : PropertyDrawer
{
	public override VisualElement CreatePropertyGUI(SerializedProperty property)
    {
		var container = new VisualElement();

		var valueField = new PropertyField(property.FindPropertyRelative(nameof(Observable<int>.Value)))
		{
			label = property.name,
			
		};
		
		container.Add(valueField);
		return container;
	}
}